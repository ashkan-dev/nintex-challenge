﻿using interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace daos.dao.services
{
    public class ShortenUrlDao: IShortenExecuter
    {
        public string ICreateShortenUrl => "INSERT INTO [dbo].[ShortUrl] " +
           "([MainUrl]" +
           ",[ProvidedUrl]" +
           ",[LastUpdDt])" +
          " VALUES " +
           "(@MainUrl" +
           ",@ProvidedUrl" +
           ",GETDATE())";

        public string IReadShortenUrl => "SELECT ProvidedUrl FROM [dbo].[ShortUrl] WHERE [MainUrl] = @MainUrl";
    }
}
