﻿using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using interfaces.aws.ops;
using System;
using System.Collections.Generic;
using System.Text;

namespace daos.aws.ops
{
    public class AppMsgs: IAppMsgs
    {
        private readonly basics basics;

        public AppMsgs(basics basics)
        {
            this.basics = basics;
        }

        public void SendNaturalMsg(string msg, string nameSpace, string method)
        {
            var client = new AmazonSQSClient(basics.Credentials.AccessKey, basics.Credentials.SecretKey, RegionEndpoint.APSoutheast1);
            string msgBody = string.Format("Method: {0} > {1} - Msg: {2}", nameSpace, method, msg);

            var req = new SendMessageRequest
            {
                MessageBody = msgBody,
                QueueUrl = basics.LogUrl
            };
            SendMessageResponse response = client.SendMessageAsync(req).Result;
        }

        public void SendErrMsg(string msg, string nameSpace, string method)
        {
            var client = new AmazonSQSClient(RegionEndpoint.APSoutheast1);
            string msgBody = string.Format("Method: {0} > {1} - Error: {2}", nameSpace, method, msg);

            var req = new SendMessageRequest
            {
                MessageBody = msgBody,
                QueueUrl = basics.LogUrl
            };
            SendMessageResponse response = client.SendMessageAsync(req).Result;
        }
    }
}
