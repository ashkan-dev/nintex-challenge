﻿using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using entities.Constants;
using entities.models;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace daos.aws.ops
{
    public class basics
    {
        private static ImmutableCredentials credentials { get; set; }
        private static string dbCon { get; set; }
        private static string sqsUrl = "https://sqs.ap-southeast-1.amazonaws.com/734219682418/nintex-backend-logs";
        
        public ImmutableCredentials Credentials
        {
            get
            {
                return credentials;
            }
        }

        public string DbCon
        {
            get
            {
                return dbCon;
            }
        }

        public string LogUrl
        {
            get
            {
                return sqsUrl;
            }
        }

        public bool connect(AccessKeys model)
        {
            AWSCredentials iam = ConnectToAWS(model);
            credentials = iam.GetCredentials();
            return true;
        }

        private AWSCredentials ConnectToAWS(AccessKeys model)
        {
            string accessKey = model.id;
            string secretKey = model.secret;

            var options = new CredentialProfileOptions
            {
                AccessKey = accessKey,
                SecretKey = secretKey
            };
            var profile = new CredentialProfile("basic_profile", options);
            profile.Region = RegionEndpoint.APSoutheast1;
            var sharedFile = new SharedCredentialsFile();
            sharedFile.RegisterProfile(profile);
            return profile.GetAWSCredentials(sharedFile);
        }

        public bool GetConStr()
        {
            var client = new AmazonSimpleSystemsManagementClient(credentials.AccessKey, credentials.SecretKey, RegionEndpoint.APSoutheast1);
            string name = "nintex-challenge-con-str";

            var req = new GetParameterRequest
            {
                Name = name,
                WithDecryption = true
            };
            GetParameterResponse response = client.GetParameterAsync(req).Result;

            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception(ErrContants.AWS_SERVICE_ERR);
            }
            dbCon = response.Parameter.Value;
            return true;
        }
    }
}
