﻿using daos.aws.ops;
using Dapper;
using entities.Constants;
using interfaces.aws.ops;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace daos
{
    public class BaseProxy
    {
        private readonly basics basics;
        private readonly IAppMsgs appMsgs;

        public BaseProxy(
            basics basics,
            IAppMsgs appMsgs
            )
        {
            this.basics = basics;
            this.appMsgs = appMsgs;
        }

        public bool CreateItem(string cmd, object param)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(basics.DbCon))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();

                    int result = con.Execute(cmd, param);

                    if (result == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public T ReadItem<T>(string cmd, object param)
        {
            T data;
            appMsgs.SendNaturalMsg(LogMsgs.READ_DB, GetType().FullName, MethodBase.GetCurrentMethod().Name);
            using (IDbConnection con = new SqlConnection(basics.DbCon))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                data = con.ExecuteScalar<T>(cmd, param);
            }
            appMsgs.SendNaturalMsg(LogMsgs.READ_DB_COMPLETE, GetType().FullName, MethodBase.GetCurrentMethod().Name);
            return data;
        }
    }
}
