﻿using business.rules.interfaces;
using entities.dtos;
using interfaces.aws.ops;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace challenge.test
{
    [TestClass]
    public class UrlShortenTest: BaseTest
    {
        private IAwsOps AwsService;
        private IUrlOps UrlService;

        [TestInitialize]
        public void Initialize()
        {
            InitializeDependencies();
        }

        private void InitializeDependencies()
        {
            try
            {
                this.AwsService = webHost.Services.GetService<IAwsOps>();
                this.UrlService = webHost.Services.GetService<IUrlOps>();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [TestMethod]
        public void ShortenTest()
        {
            var model = new AccessKeysReq
            {
                id = "AKIA2V4XHIJZFUOJXLHA",
                secret = "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"
            };

            try
            {
                var connected = AwsService.connect(model);
                Assert.IsTrue(connected);
                
                if (connected)
                {
                    var response = new UrlShortenResponse();
                    response = UrlService.ShortenUrl("http://www.facebook.com");
                    Assert.IsNotNull(response.ProvidedUrl);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}
