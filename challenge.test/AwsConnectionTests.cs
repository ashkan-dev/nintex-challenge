﻿using entities.dtos;
using interfaces.aws.ops;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace challenge.test
{
    [TestClass]
    public class AwsConnectionTests : BaseTest
    {
        private IAwsOps AwsService;

        [TestInitialize]
        public void Initialize()
        {
            InitializeDependencies();
        }

        private void InitializeDependencies()
        {
            try
            {
                this.AwsService = webHost.Services.GetService<IAwsOps>();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [TestMethod]
        public void ConnectTest()
        {
            var model = new AccessKeysReq
            {
                id = "AKIA2V4XHIJZFUOJXLHA",
                secret = "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"
            };

            try
            {
                var connected = AwsService.connect(model);
                Assert.IsTrue(connected);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}
