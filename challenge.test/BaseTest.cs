﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using nintex.challenge;
using System;
using System.Collections.Generic;
using System.Text;

namespace challenge.test
{
    [TestClass]
    public class BaseTest
    {
        public IWebHost webHost;

        public BaseTest()
        {
            webHost = WebHost
                .CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build();
            webHost.Start();
        }
    }
}
