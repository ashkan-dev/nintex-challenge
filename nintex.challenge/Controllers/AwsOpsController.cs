﻿using entities.dtos;
using interfaces.aws.ops;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nintex.challenge.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AwsOpsController: ControllerBase
    {
        private readonly IAwsOps awsOps;

        public AwsOpsController(IAwsOps awsOps)
        {
            this.awsOps = awsOps;
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult GetCredentials(AccessKeysReq req)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(req);
            }
            var response = awsOps.connect(req);
            return Ok(response);
        }
    }
}
