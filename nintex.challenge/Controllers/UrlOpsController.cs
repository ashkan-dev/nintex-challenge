﻿using business.rules;
using business.rules.interfaces;
using entities.dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nintex.challenge.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UrlOpsController: ControllerBase
    {
        private readonly IUrlOps urlOps;

        public UrlOpsController(
            IUrlOps UrlOps
            )
        {
            urlOps = UrlOps;
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult shortener(UrlShortenReq req)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(req);
            }
            var response = urlOps.ShortenUrl(req.url);
            return Ok(response);
        }
    }
}
