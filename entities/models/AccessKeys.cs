﻿using System;
using System.Collections.Generic;
using System.Text;

namespace entities.models
{
    public class AccessKeys
    {
        public string id { get; set; }
        public string secret { get; set; }
    }
}
