﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace entities.models
{
    public class ShortUrl
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string MainUrl { get; set; }
        [Required]
        public string ProvidedUrl { get; set; }
        [Required]
        public DateTime LastUpdDt { get; set; }
    }
}
