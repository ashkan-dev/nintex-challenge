﻿using System;
using System.Collections.Generic;
using System.Text;

namespace entities.dtos
{
    public class AccessKeysReq
    {
        public string id { get; set; }
        public string secret { get; set; }
    }
}
