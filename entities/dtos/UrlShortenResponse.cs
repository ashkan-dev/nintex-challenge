﻿using System;
using System.Collections.Generic;
using System.Text;

namespace entities.dtos
{
    public class UrlShortenResponse
    {
        public string ProvidedUrl { get; set; }
    }
}
