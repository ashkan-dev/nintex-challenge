﻿using System;
using System.Collections.Generic;
using System.Text;

namespace entities.dtos
{
    public class UrlShortenReq
    {
        public string url { get; set; }
    }
}
