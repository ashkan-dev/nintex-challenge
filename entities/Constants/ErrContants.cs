﻿using System;
using System.Collections.Generic;
using System.Text;

namespace entities.Constants
{
    public class ErrContants
    {
        public const string URL_NOT_VALID = "The url you entered is not valid";
        public const string AWS_SERVICE_ERR = "There is an error in using aws service. Please try later.";
    }
}
