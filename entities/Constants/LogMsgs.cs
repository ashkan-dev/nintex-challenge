﻿using System;
using System.Collections.Generic;
using System.Text;

namespace entities.Constants
{
    public class LogMsgs
    {
        public const string START_AWS_CONNECTING = "Start connecting to aws";
        public const string AWS_PROFILE_RECEIVED = "AWS profile received successfully";
        public const string AWS_CREDENTIALS_RECEIVED = "AWS credentials received successfully";
        public const string CON_STR_AWS = "Start getting connection string from aws";
        public const string CON_STR_RECEIVED = "Connection string received successfully";
        public const string VALID_URL = "Start validating main url";
        public const string VALID_URL_FINISH = "validating main url finished";
        public const string READ_DB = "Start reading from db";
        public const string READ_DB_COMPLETE = "Get db data completed successfully";
        public const string SHORT_URL = "Start shortening url";
        public const string PROVIDED_URL = "Shorten url provided";
    }
}
