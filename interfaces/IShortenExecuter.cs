﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interfaces
{
    public interface IShortenExecuter
    {
        string ICreateShortenUrl { get; }
        string IReadShortenUrl { get; }
    }
}
