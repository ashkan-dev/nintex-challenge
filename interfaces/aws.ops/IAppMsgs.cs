﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interfaces.aws.ops
{
    public interface IAppMsgs
    {
        void SendNaturalMsg(string msg, string nameSpace, string method);
        void SendErrMsg(string msg, string nameSpace, string method);
    }
}
