﻿using entities.dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace interfaces.aws.ops
{
    public interface IAwsOps
    {
        bool connect(AccessKeysReq model);
    }
}
