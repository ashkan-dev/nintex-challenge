﻿using daos.aws.ops;
using entities.dtos;
using entities.models;
using interfaces.aws.ops;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace business.rules
{
    public class AwsOpsService: IAwsOps
    {
        private readonly basics basics;
        private readonly IAppMsgs appMsgs;

        public AwsOpsService(
            basics basics,
            IAppMsgs appMsgs
            )
        {
            this.basics = basics;
            this.appMsgs = appMsgs;
        }

        public bool connect(AccessKeysReq req)
        {
            bool result;
            var model = new AccessKeys
            {
                id = req.id,
                secret = req.secret
            };

            try
            {
                result = basics.connect(model);
                
                if (result)
                {
                    result = basics.GetConStr();
                }
            }
            catch (Exception ex)
            {
                appMsgs.SendErrMsg(ex.Message, GetType().FullName, MethodBase.GetCurrentMethod().Name);
                throw;
            }

            return result;
        }
    }
}
