﻿using business.rules.helpers;
using business.rules.interfaces;
using daos.aws.ops;
using daos.dao.services;
using interfaces;
using interfaces.aws.ops;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace business.rules
{
    public static class extensions
    {
        public static IServiceCollection AddServiceDependencies(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            services.AddSingleton<IAppMsgs, AppMsgs>();
            services.AddSingleton<IShortenExecuter, ShortenUrlDao>();
            services.AddTransient<IUrlOps, UrlOpsService>();
            services.AddTransient<IAwsOps, AwsOpsService>();
            services.AddTransient<IUrl, Url>();
            return services;
        }
    }
}
