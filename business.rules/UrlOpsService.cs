﻿using business.rules.interfaces;
using entities.Constants;
using business.rules.helpers;
using System;
using System.Collections.Generic;
using System.Text;
using interfaces;
using daos;
using entities.models;
using entities.CmdParams;
using entities.dtos;
using System.Reflection;
using interfaces.aws.ops;

namespace business.rules
{
    public class UrlOpsService : IUrlOps
    {
        private readonly BaseProxy baseProxy;
        private readonly IShortenExecuter commands;
        private readonly IUrl urlTools;
        private readonly IAppMsgs appMsgs;

        public UrlOpsService(
            BaseProxy baseProxy,
            IShortenExecuter commands,
            IUrl urlTools,
            IAppMsgs appMsgs
            )
        {
            this.baseProxy = baseProxy;
            this.commands = commands;
            this.urlTools = urlTools;
            this.appMsgs = appMsgs;
        }

        public UrlShortenResponse ShortenUrl(string url)
        {
            var response = new UrlShortenResponse();
            try
            {
                if (!urlTools.validator(url))
                {
                    throw new Exception(ErrContants.URL_NOT_VALID);
                }

                var param = new ReadUrlParams
                {
                    MainUrl = new Uri(url).Host
                };
                var data = baseProxy.ReadItem<string>(commands.IReadShortenUrl, param);
                
                if (data != null)
                {
                    response.ProvidedUrl = data;
                    return response;
                }
                
                response.ProvidedUrl = urlTools.shortener(url);

                var model = new ShortUrl
                {
                    MainUrl = param.MainUrl,
                    ProvidedUrl = response.ProvidedUrl
                };
                bool inserted = baseProxy.CreateItem(commands.ICreateShortenUrl, model);
            }
            catch (Exception ex)
            {
                appMsgs.SendErrMsg(ex.Message, GetType().FullName, MethodBase.GetCurrentMethod().Name);
                throw;
            }

            return response;
        }
    }
}
