﻿using business.rules.interfaces;
using entities.Constants;
using interfaces.aws.ops;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace business.rules.helpers
{
    public class Url: IUrl
    {
        private readonly IAppMsgs appMsgs;

        public Url(IAppMsgs appMsgs)
        {
            this.appMsgs = appMsgs;
        }

        public bool validator(string url)
        {
            appMsgs.SendNaturalMsg(LogMsgs.VALID_URL, GetType().FullName, MethodBase.GetCurrentMethod().Name);
            url = WellFormed(url);
            bool result = Uri.IsWellFormedUriString(url, UriKind.Absolute);
            appMsgs.SendNaturalMsg(string.Format("{0} - Valid: {1}", LogMsgs.VALID_URL_FINISH, result), GetType().FullName, MethodBase.GetCurrentMethod().Name);
            return result;
        }

        public string shortener(string url)
        {
            appMsgs.SendNaturalMsg(LogMsgs.SHORT_URL, GetType().FullName, MethodBase.GetCurrentMethod().Name);
            url = WellFormed(url);
            var uri = new Uri(url);
            url = uri.Host;

            if (url.StartsWith("www."))
            {
                url = url.Replace("www.", "");
            }
            else if (url.StartsWith("www2."))
            {
                url = url.Replace("www2.", "");
            }
            string[] SplitedDomain = url.Split('.');
            string SecondLevel = SplitedDomain[0];
            string urlsafe = string.Empty;

            SecondLevel
              .Where(i => i < 58 || i > 64 && i < 91 || i > 96)
              .OrderBy(o => new Random().Next())
              .ToList()
              .ForEach(i => urlsafe += Convert.ToChar(i));
            int random = new Random().Next(0, urlsafe.Length);
            string provided = string.Empty;
            try
            {
                provided = urlsafe.Substring(random, new Random().Next(random, (urlsafe.Length - 1)));
                provided = string.Format("{0}.{1}", provided, string.Join('.', SplitedDomain.Skip(1)));
            }
            catch (ArgumentOutOfRangeException ex)
            {
                provided = shortener(url);
            }

            if (string.IsNullOrEmpty(provided))
            {
                provided = shortener(url);
            }
            appMsgs.SendNaturalMsg(LogMsgs.PROVIDED_URL, GetType().FullName, MethodBase.GetCurrentMethod().Name);
            return provided;
        }

        private string WellFormed(string url)
        {
            if (!Regex.IsMatch(url, @"^https?:\/\/", RegexOptions.IgnoreCase))
                url = "http://" + url;
            return url;
        }
    }
}
