﻿using System;
using System.Collections.Generic;
using System.Text;

namespace business.rules.interfaces
{
    public interface IUrl
    {
        bool validator(string url);
        string shortener(string url);
    }
}
