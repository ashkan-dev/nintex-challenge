﻿using entities.dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace business.rules.interfaces
{
    public interface IUrlOps
    {
        UrlShortenResponse ShortenUrl(string url);
    }
}
